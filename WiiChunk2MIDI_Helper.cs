﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Ports;
using System.Reflection;
using System.Threading;
using System.Xml;
using Midi;


namespace WiiChunk2MIDI
{
    public enum ParameterType { BYTE = 0, UINT = 1, BOOL = 2 };
    public enum JoyPointType { O = 0, N = 1, NE = 2, E = 3, SE = 4, S = 5, SW = 6, W = 7, NW = 8 };
    
    public class WiiChunkParameter
    {
        private double value_l = 0;
        private double value_h = 255;

        private ParameterType value_type = ParameterType.UINT;
        public ParameterType DataType
        {
            get { return value_type; }
            set { value_type = value; }
        }

        private int value_now = 0;
        public UInt16 Value
        {
            get 
            {
                double data = 128;
                if ( this.value_type == ParameterType.BYTE )
                {
                    if ( this.value_now < this.value_center )
                    {
                        data = this.value_l * ( this.value_now - this.value_min );
                        if ( data > 128 ) data = 128;
                        else if ( data < 0 ) data = 0;
                    }
                    else if ( this.value_now > this.value_center )
                    {
                        data = 256 - this.value_h * ( this.value_max - this.value_now );
                        if ( data > 256 ) data = 256;
                        else if ( data < 128 ) data = 128;
                    }
                    else
                    {
                        data = 128;
                    }
                    if ( DataNormal )
                        return Convert.ToUInt16( filter( data ) / 2 );
                    else
                        return Convert.ToUInt16( filter( data ) );
                }
                else if ( this.value_type == ParameterType.UINT )
                {
                    if ( this.value_now < this.value_center )
                    {
                        data = this.value_l * ( this.value_now - this.value_min );
                        if ( data > 512 ) data = 512;
                        else if ( data < 0 ) data = 0;
                    }
                    else if ( this.value_now > this.value_center )
                    {
                        data = 1024 - this.value_h * ( this.value_max - this.value_now );
                        if ( data > 1024 ) data = 1024;
                        else if ( data < 512 ) data = 512;
                    }
                    else
                    {
                        data = 512;
                    }
                    if ( DataNormal )
                        return Convert.ToUInt16( filter( data ) / 8 );
                    else
                        return Convert.ToUInt16( filter( data ) );
                }
                else
                {
                    return Convert.ToUInt16( this.value_now );
                }
            }
            set 
            {
                value_last = Value;
                value_now = value; 
            }
        }

        private int value_last = 0;
        public UInt16 Last
        {
            get { return (UInt16)value_last; }
            set { value_last = value; }
        }

        private int value_cal = 0;
        public UInt16 Cal
        {
            get { return (UInt16)value_cal; }
            set { value_cal = value; }
        }

        private int value_center = 0;
        public UInt16 Center
        {
            get { return (UInt16)value_center; }
            set { value_center = value; }
        }

        private int value_min = 0;
        public UInt16 Min
        {
            get { return (UInt16)value_min; }
            set { value_min = value; }
        }

        private int value_max = 0;
        public UInt16 Max
        {
            get { return (UInt16)value_max; }
            set { value_max = value; }
        }

        private Boolean DataNormal = false;
        public Boolean DataToMIDI
        {
            get { return DataNormal; }
            set { DataNormal = value; }
        }

        private double[] lastdata = new Double[10] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
        private double filter( double data )
        {
            // first order Butterworth low-pass filter            
            // y(n) = x(n) - a1 * y(n-1)
            return ( data - 0.618 * Last );
            //return data;
        }

        public WiiChunkParameter(ParameterType type, int center=0, int min=0, int max=0)
        {
            this.value_type = type;

            this.value_center = center;
            this.value_min = min;
            this.value_max = max;

            double cen = ( max - min ) / 2;
            if ( type == ParameterType.BYTE )
            {
                this.value_cal = (int)( center - 128 );
                this.value_l = 128.0 / ( this.value_center - this.value_min );
                this.value_h = 128.0 / ( this.value_max - this.value_center );
            }
            else if ( type == ParameterType.UINT )
            {
                this.value_cal = (int)( center - 512 );
                this.value_l = 512.0 / ( this.value_center - this.value_min );
                this.value_h = 512.0 / ( this.value_max - this.value_center );
            }
        }
    }

    public static class WiiChunk
    {
        public static WiiChunkParameter JoyX = new WiiChunkParameter( ParameterType.BYTE, 152, 62, 220 ); //{ Center = 152, Min = 36, Max = 249 };
        public static WiiChunkParameter JoyY = new WiiChunkParameter( ParameterType.BYTE, 136, 49, 210 ); //{ Center = 136, Min = 23, Max = 235 };

        public static WiiChunkParameter AccelX = new WiiChunkParameter( ParameterType.UINT, 550, 0, 1024 ); //{ Center = 550, Min = 0, Max = 1023 };
        public static WiiChunkParameter AccelY = new WiiChunkParameter( ParameterType.UINT, 527, 0, 1024 ); //{ Center = 527, Min = 0, Max = 1023 };
        public static WiiChunkParameter AccelZ = new WiiChunkParameter( ParameterType.UINT, 760, 0, 1024 ); //{ Center = 760, Min = 0, Max = 1023 };

        public static WiiChunkParameter ButtonC = new WiiChunkParameter( ParameterType.BOOL ); //{ DataType = ParameterType.BOOL };
        public static WiiChunkParameter ButtonZ = new WiiChunkParameter( ParameterType.BOOL ); //{ DataType = ParameterType.BOOL };        

        internal static void LoadSetting()
        {
            String infoName = String.Format( "{0}\\{1}", Path.GetDirectoryName( Assembly.GetExecutingAssembly().ManifestModule.FullyQualifiedName ), "WiiChunk.Info" );
            XmlDocument xmldoc = new XmlDocument();

            if ( File.Exists( infoName ) )
            {
                xmldoc.Load( infoName );

                XmlNodeList elements = null;
                elements = xmldoc.GetElementsByTagName( "parameter" );
                foreach ( XmlNode element in elements )
                {
                    String pName = String.Empty;
                    int pCenter = -1;
                    int pMin = -1;
                    int pMax = -1;
                    ParameterType pType = ParameterType.UINT;

                    XmlAttributeCollection attrs = element.Attributes;
                    foreach ( XmlAttribute attr in attrs )
                    {
                        if ( String.Equals( "Name", attr.Name, StringComparison.InvariantCultureIgnoreCase ) )
                        {
                            pName = attr.Value;
                        }
                        if ( String.Equals( "Type", attr.Name, StringComparison.InvariantCultureIgnoreCase ) )
                        {
                            pType = (ParameterType)Enum.Parse( typeof( ParameterType ), attr.Value );
                        }
                        if ( String.Equals( "Center", attr.Name, StringComparison.InvariantCultureIgnoreCase ) )
                        {
                            pCenter = int.Parse( attr.Value );
                        }
                        if ( String.Equals( "Min", attr.Name, StringComparison.InvariantCultureIgnoreCase ) )
                        {
                            pMin = int.Parse( attr.Value );
                        }
                        if ( String.Equals( "Max", attr.Name, StringComparison.InvariantCultureIgnoreCase ) )
                        {
                            pMax = int.Parse( attr.Value );
                        }
                    }

                    if ( String.Equals( "JoyX", pName, StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        JoyX = new WiiChunkParameter( ParameterType.BYTE, pCenter, pMin, pMax );
                    }
                    else if ( String.Equals( "JoyY", pName, StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        JoyY = new WiiChunkParameter( ParameterType.BYTE, pCenter, pMin, pMax );
                    }
                    else if ( String.Equals( "AccelX", pName, StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        AccelX = new WiiChunkParameter( ParameterType.UINT, pCenter, pMin, pMax );
                    }
                    else if ( String.Equals( "AccelY", pName, StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        AccelY = new WiiChunkParameter( ParameterType.UINT, pCenter, pMin, pMax );
                    }
                    else if ( String.Equals( "AccelZ", pName, StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        AccelZ = new WiiChunkParameter( ParameterType.UINT, pCenter, pMin, pMax );
                    }
                    else if ( String.Equals( "ButtonZ", pName, StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        ButtonZ = new WiiChunkParameter( ParameterType.BOOL, pCenter, pMin, pMax );
                    }
                    else if ( String.Equals( "ButtonC", pName, StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        ButtonC = new WiiChunkParameter( ParameterType.BOOL, pCenter, pMin, pMax );
                    }
                }
            }
        }

        internal static String GetControllerName( Midi.Control control )
        {
            if ( control == 0 ) return "[NONE]";
            else return Enum.GetName( typeof( Midi.Control ), control );
        }

        internal static Midi.Control GetControllerValue( String control )
        {
            if ( String.IsNullOrEmpty( control ) ) return 0;
            else if ( String.Equals( "[NONE]", control, StringComparison.InvariantCultureIgnoreCase ) ) return 0;
            else return (Midi.Control)Enum.Parse( typeof( Midi.Control ), control );
        }

        internal static String GetChannelName( Midi.Channel channel )
        {
            if ( channel == 0 ) return "[NONE]";
            else return Enum.GetName( typeof( Midi.Channel ), channel );
        }

        internal static Midi.Channel GetChannelValue( String channel )
        {
            if ( String.IsNullOrEmpty( channel ) ) return 0;
            else if ( String.Equals( "[NONE]", channel, StringComparison.InvariantCultureIgnoreCase ) ) return 0;
            else return (Midi.Channel)Enum.Parse( typeof( Midi.Channel ), channel );
        }

        public static JoyPointType JoyPoint()
        {
            double cen = 128.0;
            if ( WiiChunk.DataToMIDI ) cen = 64.0;
            double x = JoyX.Value - cen;
            double y = JoyY.Value - cen;
            //double x = JoyX.Value - JoyX.Last;
            //double y = JoyY.Value - JoyY.Last;
            double r = Math.Sqrt( x * x + y * y );
            if ( r == 0 ) return JoyPointType.O;

            double a = 0;
            if ( ( y >= 0 ) && ( x >= 0 ) )
            {
                a = Math.Asin( y / r ) * 180 / Math.PI;
            }
            else if ( ( y >= 0 ) && ( x < 0 ) )
            {
                a =  Math.Acos( x / r ) * 180 / Math.PI;
            }
            else if ( ( y < 0 ) && ( x < 0 ) )
            {
                a = 180 + Math.Asin( -y / r ) * 180 / Math.PI;
            }
            else if ( ( y < 0 ) && ( x >= 0 ) )
            {
                a = 180 + Math.Acos( -x / r ) * 180 / Math.PI;
            }

            if ( a < 0 ) a = 360 + a;

            if ( ( 360 - 20 < a ) || ( a < 0 + 20 ) )
            {
                return JoyPointType.E;
            }
            else if ( ( 45 - 20 < a ) && ( a < 45 + 20 ) )
            {
                return JoyPointType.NE;
            }
            else if ( ( 90 - 20 < a ) && ( a < 90 + 20 ) )
            {
                return JoyPointType.N;
            }
            else if ( ( 135 - 20 < a ) && ( a < 135 + 20 ) )
            {
                return JoyPointType.NW;
            }
            else if ( ( 180 - 20 < a ) && ( a < 180 + 20 ) )
            {
                return JoyPointType.W;
            }
            else if ( ( 227 - 20 < a ) && ( a < 227 + 20 ) )
            {
                return JoyPointType.SW;
            }
            else if ( ( 270 - 20 < a ) && ( a < 270 + 20 ) )
            {
                return JoyPointType.S;
            }
            else if ( ( 315 - 20 < a ) && ( a < 315 + 20 ) )
            {
                return JoyPointType.SE;
            }
            return JoyPointType.O;
        }

        private static Boolean DataNormal = false;
        public static Boolean DataToMIDI
        {
            get { return DataNormal; }
            set 
            { 
                DataNormal = value;

                JoyX.DataToMIDI = value;
                JoyY.DataToMIDI = value;

                AccelX.DataToMIDI = value;
                AccelY.DataToMIDI = value;
                AccelZ.DataToMIDI = value;
            }
        }

        private static Midi.Control JoyX_FuncId = 0;
        public static String JoyX_Func
        {
            get { return GetControllerName( JoyX_FuncId ); }
            set { JoyX_FuncId = GetControllerValue( value ); }
        }
        private static Midi.Control JoyY_FuncId = 0;
        public static String JoyY_Func
        {
            get { return GetControllerName( JoyY_FuncId ); }
            set { JoyY_FuncId = GetControllerValue( value ); }
        }

        private static Midi.Control AccelX_FuncId = 0;
        public static String AccelX_Func
        {
            get { return GetControllerName( AccelX_FuncId ); }
            set { AccelX_FuncId = GetControllerValue( value ); }
        }
        private static Midi.Control AccelY_FuncId = 0;
        public static String AccelY_Func
        {
            get { return GetControllerName( AccelY_FuncId ); }
            set { AccelY_FuncId = GetControllerValue( value ); }
        }
        private static Midi.Control AccelZ_FuncId = 0;
        public static String AccelZ_Func
        {
            get { return GetControllerName( AccelZ_FuncId ); }
            set { AccelZ_FuncId = GetControllerValue( value ); }
        }

        private static Midi.Control ButtonZ_FuncId = 0;
        public static String ButtonZ_Func
        {
            get { return GetControllerName( ButtonZ_FuncId ); }
            set { ButtonZ_FuncId = GetControllerValue( value ); }
        }
        private static Midi.Control ButtonC_FuncId = 0;
        public static String ButtonC_Func
        {
            get { return GetControllerName( ButtonC_FuncId ); }
            set { ButtonC_FuncId = GetControllerValue( value ); }
        }

        private static Midi.Channel ChannelId = 0;
        public static String Channel
        {
            get { return GetChannelName( ChannelId ); }
            set { ChannelId = GetChannelValue( value ); }
        }

        //public void WiiChunk()
        //{
        //    LoadSetting();
        //}
    }

    class WiiChunk2MIDI_Helper
    {
        private UInt16 lastZPressed = 1;
        private int lastZPitch = -1;
        private int lastZVelocity = -1;
        private Midi.Channel lastZChannel = Midi.Channel.Channel1;

        private UInt16 lastCPressed = 1;
        private Midi.Channel lastCChannel = Midi.Channel.Channel1;
        private int lastCPitch = -1;
        private int lastCVelocity = -1;

        //private WiiChunkDevice WiiChunk = new WiiChunkDevice();

        #region properties
        private InputDevice mi = null;
        public String MidiIn
        {
            get 
            {
                if ( mi != null ) return mi.Name;
                //else return String.Empty;
                else return "[NONE]";
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ))
                {
                    if ( String.Equals( "[NONE]", value, StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        if ( ( mi != null ) && ( mi.IsOpen ) ) mi.Close();
                        mi = null;
                    }
                    else
                    {
                        foreach ( InputDevice device in InputDevice.InstalledDevices )
                        {
                            if ( String.Equals( device.Name, value, StringComparison.InvariantCultureIgnoreCase ) )
                            {
                                mi = device;
                                break;
                            }
                        }
                    }
                }
            }
        }
        private List<String> mi_list = new List<String>();
        public List<String> MidiInList
        {
            get { return mi_list; }
        }

        private OutputDevice mo = null;
        public String MidiOut
        {
            get
            {
                if ( mo != null ) return mo.Name;
                //else return String.Empty;
                else return "[NONE]";
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    if ( String.Equals( "[NONE]", value, StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        if ( ( mo != null ) && ( mo.IsOpen ) ) mo.Close();
                        mo = null;
                    }
                    else
                    {
                        foreach ( OutputDevice device in OutputDevice.InstalledDevices )
                        {
                            if ( String.Equals( device.Name, value, StringComparison.InvariantCultureIgnoreCase ) )
                            {
                                mo = device;
                                break;
                            }
                        }
                    }
                }
            }
        }
        private List<String> mo_list = new List<String>();
        public List<String> MidiOutList
        {
            get { return mo_list; }
        }

        private SerialPort ms = new SerialPort();
        public String Serial
        {
            get
            {
                if ( ms != null ) return ms.PortName;
                //else return String.Empty;
                else return "[NONE]";
            }
            set
            {
                if ( !String.IsNullOrEmpty( value ) )
                {
                    if ( String.Equals( "[NONE]", value, StringComparison.InvariantCultureIgnoreCase ) )
                    {
                        if ( ( ms != null ) && ( ms.IsOpen ) ) ms.Close();
                        ms.PortName = "";
                    }
                    else
                    {
                        String[] AllPorts = SerialPort.GetPortNames();
                        foreach ( String device in AllPorts )
                        {
                            if ( String.Equals( device, value, StringComparison.InvariantCultureIgnoreCase ) )
                            {
                                if ( ( ms != null ) && ( !ms.IsOpen ) )
                                {
                                    ms.PortName = value;
                                    ms.BaudRate = 115200;
                                    ms.Parity = Parity.None;
                                    ms.DataBits = 8;
                                    ms.StopBits = StopBits.One;
                                    ms.NewLine = "\n";
                                }
                                break;
                            }
                        }
                    }
                }
            }
        }
        private List<String> ms_list = new List<String>();
        public List<String> SerialList
        {
            get { return ms_list; }
        }

        private Boolean IsOpened = false;
        public Boolean IsOpen
        {
            get
            {
                Boolean state = IsOpened;
                return state;
            }
        }

        private Boolean update_device = false;
        public Boolean UpdateDevice
        {
            get 
            {
                Boolean state = update_device;
                update_device = false;
                return state; 
            }
            //set { update_device = value; }
        }

        private Boolean update_wiichunk = true;
        public Boolean UpdateWiiChunk
        {
            get 
            {
                Boolean state = update_wiichunk;
                update_wiichunk = false;
                return state; 
            }
            //set { update_wiichunk = value; }
        }

        private Boolean update_function = true;
        public Boolean UpdateFunction
        {
            get
            {
                Boolean state = update_function;
                //update_function = false;
                return state;
            }
            //set { update_function = value; }
        }

        private List<String> midi_controlllers = new List<String>();
        public String[] Controllers
        {
            get { return midi_controlllers.ToArray(); }
        }

        private List<String> midi_channels = new List<String>();
        public String[] Channels
        {
            get { return midi_channels.ToArray(); }
        }

        #endregion

        public WiiChunk2MIDI_Helper()
        {
            WiiChunk.LoadSetting();

            CheckDevice();
            this.ms.PinChanged += new System.IO.Ports.SerialPinChangedEventHandler( this.serial_PinChanged );
            this.ms.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler( this.serial_DataReceived );

            midi_controlllers.Clear();
            midi_controlllers.Add( "[NONE]" );
            midi_controlllers.AddRange( Enum.GetNames(typeof(Midi.Control )) );

            midi_channels.Clear();
            midi_channels.Add( "[NONE]" );
            midi_channels.AddRange( Enum.GetNames( typeof( Midi.Channel ) ) );
        }

        private void serial_DataReceived( object sender, SerialDataReceivedEventArgs e )
        {
            try
            {
                if ( ms.IsOpen )
                {
                    String data = ms.ReadLine();
                    String[] values = data.Split( new char[] { ',' } );
                    if ( values.Length == 7 )
                    {
                        try
                        {
                            WiiChunk.JoyX.Value = Convert.ToUInt16( values[0].Trim() );
                            WiiChunk.JoyY.Value = Convert.ToUInt16( values[1].Trim() );
                            WiiChunk.AccelX.Value = Convert.ToUInt16( values[2].Trim() );
                            WiiChunk.AccelY.Value = Convert.ToUInt16( values[3].Trim() );
                            WiiChunk.AccelZ.Value = Convert.ToUInt16( values[4].Trim() );
                            WiiChunk.ButtonZ.Value = Convert.ToUInt16( values[5].Trim() );
                            WiiChunk.ButtonC.Value = Convert.ToUInt16( values[6].Trim() );

                            SendControlToMIDI( WiiChunk.Channel );
                        }
                        catch ( Exception ee )
                        {
                            if ( ( ee is InvalidOperationException ) || ( ee is IOException ) || ( ee is UnauthorizedAccessException ) )
                            {
                                //IsOpened = false;
                            }
                        }
                    }
                    update_wiichunk = true;
                }
            }
            catch ( Exception ee )
            {

            }
        }

        private void serial_PinChanged( object sender, SerialPinChangedEventArgs e )
        {
            if ( ms.IsOpen )
            {
                ms.Close();
                ms.Open();
                update_wiichunk = true;
            }
        }

        public void CheckDevice()
        {
            String mi_name = MidiIn;
            String mo_name = MidiOut;
            String ms_name = Serial;

            mi_list.Clear();
            mi_list.Add( "[NONE]" );
            foreach ( InputDevice device in InputDevice.InstalledDevices )
            {
                mi_list.Add( device.Name );
            }

            mo_list.Clear();
            mo_list.Add( "[NONE]" );
            foreach ( OutputDevice device in OutputDevice.InstalledDevices )
            {
                mo_list.Add( device.Name );
            }

            ms_list.Clear();
            ms_list.Add( "[NONE]" );
            foreach ( String device in SerialPort.GetPortNames() )
            {
                ms_list.Add( device );
            }

            update_device = true;
        }

        public void CheckDeviceAvaliable()
        {
            if ( ( mi != null ) && ( !mi_list.Contains( mi.Name ) ) )
            {
                if ( mi.IsOpen ) mi.Close();
            }
            if ( ( mo != null ) && ( !mo_list.Contains( mo.Name ) ) )
            {
                if ( mo.IsOpen ) mo.Close();
            }
            if ( ( ms != null ) && ( !ms_list.Contains( ms.PortName ) ) )
            {
                if ( ms.IsOpen ) ms.Close();
            }
            update_device = true;
        }

        public void OpenDevice()
        {
            OpenDevice( MidiIn, MidiOut, Serial );
        }

        public void OpenDevice( String mi_name, String mo_name, String ms_name )
        {
            try
            {
                if ( !String.IsNullOrEmpty( mi_name ) )
                {
                    foreach ( InputDevice device in InputDevice.InstalledDevices )
                    {
                        if ( String.Equals( device.Name, mi_name, StringComparison.InvariantCultureIgnoreCase ) )
                        {
                            mi = device;
                            if ( ( mi != null ) && ( !mi.IsOpen ) )
                            {
                                mi.Open();
                            }
                            break;
                        }
                    }
                }

                if ( !String.IsNullOrEmpty( mo_name ) )
                {
                    foreach ( OutputDevice device in OutputDevice.InstalledDevices )
                    {
                        if ( String.Equals( device.Name, mo_name, StringComparison.InvariantCultureIgnoreCase ) )
                        {
                            mo = device;
                            if ( ( mo != null ) && ( !mo.IsOpen ) )
                            {
                                mo.Open();
                            }
                            break;
                        }
                    }
                }

                try
                {
                    if ( !String.IsNullOrEmpty( ms_name ) )
                    {
                        foreach ( String device in SerialPort.GetPortNames() )
                        {
                            if ( String.Equals( device, ms_name, StringComparison.InvariantCultureIgnoreCase ) )
                            {
                                if ( ( ms != null ) && ( !ms.IsOpen ) )
                                {
                                    ms.PortName = ms_name;
                                    ms.BaudRate = 38400;
                                    ms.Parity = Parity.None;
                                    ms.DataBits = 8;
                                    ms.StopBits = StopBits.One;
                                    ms.NewLine = "\n";

                                    ms.Open();
                                }
                                break;
                            }
                        }
                    }
                }
                catch ( Exception ee )
                {
                    if ( ( ee is InvalidOperationException ) || ( ee is IOException ) || ( ee is UnauthorizedAccessException ) )
                    {
                        IsOpened = false;
                    }
                }
                IsOpened = true;
            }
            catch ( Exception ee )
            {
                IsOpened = false;
            }
        }

        public void CloseDevice()
        {
            try
            {
                if ( ( mi != null ) && ( mi.IsOpen ) )
                {
                    mi.Close();
                }
                if ( ( mo != null ) && ( mo.IsOpen ) )
                {
                    mo.Close();
                }

                try
                {
                    if ( ( ms != null ) && ( ms.IsOpen ) )
                    {
                        ms.Close();
                    }
                }
                catch ( Exception ee )
                {
                    if ( ( ee is InvalidOperationException ) || ( ee is IOException ) || ( ee is UnauthorizedAccessException ) )
                    {

                    }
                }
            }
            catch ( Exception ee )
            {

            }
            IsOpened = false;
        }

        private void delay(int ms)
        {
            Thread.Sleep(ms);
        }

        public void SendControlToMIDI( String channel )
        {
            SendControlToMIDI( (Channel)( WiiChunk.GetChannelValue(channel)) );
        }

        public void SendControlToMIDI( int channel )
        {
            SendControlToMIDI( (Channel)(channel-1) );
        }

        public void SendControlToMIDI( Channel channel)
        {
            if ( ( mo != null ) && ( mo.IsOpen ) )
            {
                Midi.Control control = 0;

                JoyPointType joyPoint = WiiChunk.JoyPoint();

                control = WiiChunk.GetControllerValue( WiiChunk.JoyX_Func );
                if ( control != 0 )
                {
                    int volume = WiiChunk.JoyX.Value - 1;
                    if ( volume > 127 ) volume = 127;
                    if ( volume < 0 ) volume = 0;
                    if ( Math.Abs( volume - WiiChunk.JoyX.Last ) > 1 )
                    {
                        if ( ( joyPoint != JoyPointType.N ) && ( joyPoint != JoyPointType.S ) )
                        {
                            mo.SendControlChange( channel, control, volume );
                        }
                    }
                }
                control = WiiChunk.GetControllerValue( WiiChunk.JoyY_Func );
                if ( control != 0 )
                {
                    int volume = WiiChunk.JoyY.Value - 1;
                    if ( volume > 127 ) volume = 127;
                    if ( volume < 0 ) volume = 0;
                    if ( Math.Abs( volume - WiiChunk.JoyY.Last ) > 1 )
                    {
                        if ( ( joyPoint != JoyPointType.E ) && ( joyPoint != JoyPointType.W ) )
                        {
                            mo.SendControlChange( channel, control, volume );
                        }
                    }
                }

                control = WiiChunk.GetControllerValue( WiiChunk.AccelX_Func );
                if ( control != 0 )
                {
                    int volume = WiiChunk.AccelX.Value - 1;
                    if ( volume > 127 ) volume = 127;
                    if ( volume < 0 ) volume = 0;
                    if ( Math.Abs( volume - WiiChunk.AccelX.Last ) > 1 )
                    {
                        mo.SendControlChange( channel, control, volume );
                    }
                }
                control = WiiChunk.GetControllerValue( WiiChunk.AccelY_Func );
                if ( control != 0 )
                {
                    int volume = WiiChunk.AccelY.Value - 1;
                    if ( volume > 127 ) volume = 127;
                    if ( volume < 0 ) volume = 0;
                    if ( Math.Abs( volume - WiiChunk.AccelY.Last ) > 1 )
                    {
                        mo.SendControlChange( channel, control, volume );
                    }
                }
                control = WiiChunk.GetControllerValue( WiiChunk.AccelZ_Func );
                if ( control != 0 )
                {
                    int volume = WiiChunk.AccelZ.Value - 1;
                    if ( volume > 127 ) volume = 127;
                    if ( volume < 0 ) volume = 0;
                    if ( Math.Abs( volume - WiiChunk.AccelZ.Last ) > 1 )
                    {
                        mo.SendControlChange( channel, control, volume );
                    }
                }

                if ( WiiChunk.ButtonZ.Value == 0 )
                {
                    if ( WiiChunk.ButtonZ.Value != lastZPressed )
                    {
                        lastZChannel = channel;

                        int volume = WiiChunk.JoyY.Value - 1;
                        if ( volume > 127 ) volume = 127;
                        if ( volume < 0 ) volume = 0;
                        lastZVelocity = volume;

                        int pitch = WiiChunk.JoyX.Value - 1;
                        if ( pitch > 127 ) pitch = 127;
                        if ( pitch < 0 ) pitch = 0;
                        lastZPitch = pitch;

                        mo.SendNoteOn( channel, Pitch.C4, 127 );
                        //delay( 50 );
                        mo.SendNoteOff( channel, Pitch.C4, 127 );
                        lastZPressed = 0;
                    }
                    control = WiiChunk.GetControllerValue( WiiChunk.ButtonZ_Func );
                    if ( control != 0 )
                    {
                        mo.SendControlChange( channel, control, 127 );
                    }
                }
                else
                {
                    if ( WiiChunk.ButtonZ.Value != lastZPressed )
                    {
                        mo.SendNoteOff( lastZChannel, (Pitch)lastZPitch, lastZVelocity );
                        lastZPressed = 1;
                    }
                }
                if ( WiiChunk.ButtonC.Value == 0 )
                {
                    if ( WiiChunk.ButtonC.Value != lastCPressed )
                    {
                        lastCChannel = channel;

                        int volume = WiiChunk.JoyY.Value - 1;
                        if ( volume > 127 ) volume = 127;
                        if ( volume < 0 ) volume = 0;
                        lastCVelocity = volume;

                        int pitch = WiiChunk.JoyX.Value - 1;
                        if ( pitch > 127 ) pitch = 127;
                        if ( pitch < 0 ) pitch = 0;
                        lastCPitch = pitch;

                        mo.SendNoteOn( channel, (Pitch)pitch, volume );
                        //delay( 100 );
                        //mo.SendNoteOff( channel, (Pitch)lastPitch, lastVelocity );
                        lastCPressed = 0;
                    }

                    control = WiiChunk.GetControllerValue( WiiChunk.ButtonC_Func );
                    if ( control != 0 )
                    {
                        mo.SendControlChange( channel, control, 127 );
                    }
                }
                else
                {
                    if ( WiiChunk.ButtonC.Value != lastCPressed )
                    {
                        mo.SendNoteOff( lastCChannel, (Pitch)lastCPitch, lastCVelocity );
                        lastCPressed = 1;
                    }
                }
            }
        }

        public void SendControlToMIDI( uint channel, String control, int value )
        {
            SendControlToMIDI( (Channel)(channel-1), (Control)Enum.Parse( typeof( Control ), control ), value );
        }

        public void SendControlToMIDI( Channel channel, Control control, int value )
        {
            if ( ( mo != null ) && ( mo.IsOpen ) )
            {
                //int bend = ( WiiChunk.JoyX - WiiChunk.JoyX_Cal + 1 ) * 64;
                int bend = WiiChunk.JoyX.Value;
                if ( bend > 16384 ) bend = 16384;
                if ( bend < 0 ) bend = 0;
                mo.SendPitchBend( Channel.Channel1, bend );

                //int volume = ( WiiChunk.JoyY - WiiChunk.JoyY_Cal + 1 ) / 2;
                int volume = WiiChunk.JoyY.Value;
                if ( volume > 127 ) volume = 127;
                if ( volume < 0 ) volume = 0;
                mo.SendControlChange( Channel.Channel1, Midi.Control.SustainPedal, volume );

                if ( WiiChunk.ButtonZ.Value == 0 )
                {

                }
            }
        }

    }
}
