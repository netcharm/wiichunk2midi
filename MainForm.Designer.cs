﻿namespace WiiChunk2MIDI
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose( bool disposing )
        {
            if ( disposing && ( components != null ) )
            {
                components.Dispose();
            }
            base.Dispose( disposing );
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.grpDevice = new System.Windows.Forms.GroupBox();
            this.VPanelDevice = new System.Windows.Forms.FlowLayoutPanel();
            this.lblDeviceIn = new System.Windows.Forms.Label();
            this.cbDeviceIn = new System.Windows.Forms.ComboBox();
            this.lblDeviceOut = new System.Windows.Forms.Label();
            this.cbDeviceOut = new System.Windows.Forms.ComboBox();
            this.lblSerial = new System.Windows.Forms.Label();
            this.cbSerial = new System.Windows.Forms.ComboBox();
            this.btnDeviceDetect = new System.Windows.Forms.Button();
            this.grpWiiChunk = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblAngleState = new System.Windows.Forms.Label();
            this.cbWiiChunkButtonC_Func = new System.Windows.Forms.ComboBox();
            this.cbWiiChunkButtonZ_Func = new System.Windows.Forms.ComboBox();
            this.cbWiiChunkAccelZ_Func = new System.Windows.Forms.ComboBox();
            this.cbWiiChunkAccelY_Func = new System.Windows.Forms.ComboBox();
            this.cbWiiChunkAccelX_Func = new System.Windows.Forms.ComboBox();
            this.cbWiiChunkJoyY_Func = new System.Windows.Forms.ComboBox();
            this.lblWiiChunkButtonC = new System.Windows.Forms.Label();
            this.lblWiiChunkButtonC_State = new System.Windows.Forms.Label();
            this.lblWiiChunkButtonZ = new System.Windows.Forms.Label();
            this.lblWiiChunkButtonZ_State = new System.Windows.Forms.Label();
            this.lblWiiChunkAccelY_Value = new System.Windows.Forms.Label();
            this.lblWiiChunkAccelZ_Value = new System.Windows.Forms.Label();
            this.lblWiiChunkAccelX_Value = new System.Windows.Forms.Label();
            this.lblWiiChunkJoyY_Value = new System.Windows.Forms.Label();
            this.lblWiiChunkJoyX_Value = new System.Windows.Forms.Label();
            this.lblWiiChunkAccelZ = new System.Windows.Forms.Label();
            this.lblWiiChunkAccelY = new System.Windows.Forms.Label();
            this.lblWiiChunkAccelX = new System.Windows.Forms.Label();
            this.lblWiiChunkJoyY = new System.Windows.Forms.Label();
            this.lblWiiChunkJoyX = new System.Windows.Forms.Label();
            this.cbWiiChunkJoyX_Func = new System.Windows.Forms.ComboBox();
            this.lblWiiChunkChannel = new System.Windows.Forms.Label();
            this.cbWiiChunkChannel = new System.Windows.Forms.ComboBox();
            this.checkDataNormalize = new System.Windows.Forms.CheckBox();
            this.lblAngle = new System.Windows.Forms.Label();
            this.status = new System.Windows.Forms.StatusStrip();
            this.StatusLabel_Serial = new System.Windows.Forms.ToolStripStatusLabel();
            this.btnActions = new System.Windows.Forms.ToolStripSplitButton();
            this.mnActions = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.miActionsOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.miActionsClose = new System.Windows.Forms.ToolStripMenuItem();
            this.bgWork = new System.ComponentModel.BackgroundWorker();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.grpDevice.SuspendLayout();
            this.VPanelDevice.SuspendLayout();
            this.grpWiiChunk.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.status.SuspendLayout();
            this.mnActions.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpDevice
            // 
            this.grpDevice.Controls.Add(this.VPanelDevice);
            this.grpDevice.Location = new System.Drawing.Point(12, 12);
            this.grpDevice.Name = "grpDevice";
            this.grpDevice.Size = new System.Drawing.Size(240, 255);
            this.grpDevice.TabIndex = 1;
            this.grpDevice.TabStop = false;
            this.grpDevice.Text = "Device";
            // 
            // VPanelDevice
            // 
            this.VPanelDevice.Controls.Add(this.lblDeviceIn);
            this.VPanelDevice.Controls.Add(this.cbDeviceIn);
            this.VPanelDevice.Controls.Add(this.lblDeviceOut);
            this.VPanelDevice.Controls.Add(this.cbDeviceOut);
            this.VPanelDevice.Controls.Add(this.lblSerial);
            this.VPanelDevice.Controls.Add(this.cbSerial);
            this.VPanelDevice.Controls.Add(this.btnDeviceDetect);
            this.VPanelDevice.Dock = System.Windows.Forms.DockStyle.Fill;
            this.VPanelDevice.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.VPanelDevice.Location = new System.Drawing.Point(3, 17);
            this.VPanelDevice.Name = "VPanelDevice";
            this.VPanelDevice.Size = new System.Drawing.Size(234, 235);
            this.VPanelDevice.TabIndex = 4;
            this.VPanelDevice.WrapContents = false;
            // 
            // lblDeviceIn
            // 
            this.lblDeviceIn.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDeviceIn.Location = new System.Drawing.Point(8, 4);
            this.lblDeviceIn.Margin = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.lblDeviceIn.Name = "lblDeviceIn";
            this.lblDeviceIn.Size = new System.Drawing.Size(218, 20);
            this.lblDeviceIn.TabIndex = 4;
            this.lblDeviceIn.Text = "MIDI In:";
            this.lblDeviceIn.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbDeviceIn
            // 
            this.cbDeviceIn.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbDeviceIn.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeviceIn.FormattingEnabled = true;
            this.cbDeviceIn.Location = new System.Drawing.Point(8, 32);
            this.cbDeviceIn.Margin = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.cbDeviceIn.Name = "cbDeviceIn";
            this.cbDeviceIn.Size = new System.Drawing.Size(218, 20);
            this.cbDeviceIn.TabIndex = 5;
            // 
            // lblDeviceOut
            // 
            this.lblDeviceOut.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblDeviceOut.Location = new System.Drawing.Point(8, 60);
            this.lblDeviceOut.Margin = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.lblDeviceOut.Name = "lblDeviceOut";
            this.lblDeviceOut.Size = new System.Drawing.Size(218, 20);
            this.lblDeviceOut.TabIndex = 6;
            this.lblDeviceOut.Text = "MIDI Out:";
            this.lblDeviceOut.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbDeviceOut
            // 
            this.cbDeviceOut.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbDeviceOut.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDeviceOut.FormattingEnabled = true;
            this.cbDeviceOut.Location = new System.Drawing.Point(8, 88);
            this.cbDeviceOut.Margin = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.cbDeviceOut.Name = "cbDeviceOut";
            this.cbDeviceOut.Size = new System.Drawing.Size(218, 20);
            this.cbDeviceOut.TabIndex = 7;
            // 
            // lblSerial
            // 
            this.lblSerial.Dock = System.Windows.Forms.DockStyle.Top;
            this.lblSerial.Location = new System.Drawing.Point(8, 116);
            this.lblSerial.Margin = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.lblSerial.Name = "lblSerial";
            this.lblSerial.Size = new System.Drawing.Size(218, 20);
            this.lblSerial.TabIndex = 9;
            this.lblSerial.Text = "Serial Port:";
            this.lblSerial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbSerial
            // 
            this.cbSerial.Dock = System.Windows.Forms.DockStyle.Top;
            this.cbSerial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSerial.FormattingEnabled = true;
            this.cbSerial.Location = new System.Drawing.Point(8, 144);
            this.cbSerial.Margin = new System.Windows.Forms.Padding(8, 4, 8, 4);
            this.cbSerial.Name = "cbSerial";
            this.cbSerial.Size = new System.Drawing.Size(218, 20);
            this.cbSerial.TabIndex = 10;
            // 
            // btnDeviceDetect
            // 
            this.btnDeviceDetect.Location = new System.Drawing.Point(3, 192);
            this.btnDeviceDetect.Margin = new System.Windows.Forms.Padding(3, 24, 3, 3);
            this.btnDeviceDetect.Name = "btnDeviceDetect";
            this.btnDeviceDetect.Size = new System.Drawing.Size(228, 28);
            this.btnDeviceDetect.TabIndex = 8;
            this.btnDeviceDetect.Text = "Auto Detect";
            this.btnDeviceDetect.UseVisualStyleBackColor = true;
            this.btnDeviceDetect.Click += new System.EventHandler(this.btnDeviceDetect_Click);
            // 
            // grpWiiChunk
            // 
            this.grpWiiChunk.Controls.Add(this.tableLayoutPanel1);
            this.grpWiiChunk.Location = new System.Drawing.Point(258, 12);
            this.grpWiiChunk.Name = "grpWiiChunk";
            this.grpWiiChunk.Size = new System.Drawing.Size(294, 255);
            this.grpWiiChunk.TabIndex = 2;
            this.grpWiiChunk.TabStop = false;
            this.grpWiiChunk.Text = "Wii Nunchunk Parameters";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblAngleState, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.cbWiiChunkButtonC_Func, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.cbWiiChunkButtonZ_Func, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.cbWiiChunkAccelZ_Func, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.cbWiiChunkAccelY_Func, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.cbWiiChunkAccelX_Func, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.cbWiiChunkJoyY_Func, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkButtonC, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkButtonC_State, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkButtonZ, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkButtonZ_State, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkAccelY_Value, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkAccelZ_Value, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkAccelX_Value, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkJoyY_Value, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkJoyX_Value, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkAccelZ, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkAccelY, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkAccelX, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkJoyY, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkJoyX, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.cbWiiChunkJoyX_Func, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblWiiChunkChannel, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.cbWiiChunkChannel, 2, 7);
            this.tableLayoutPanel1.Controls.Add(this.checkDataNormalize, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblAngle, 0, 8);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 17);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(288, 235);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblAngleState
            // 
            this.lblAngleState.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAngleState.Location = new System.Drawing.Point(76, 196);
            this.lblAngleState.Margin = new System.Windows.Forms.Padding(4);
            this.lblAngleState.Name = "lblAngleState";
            this.lblAngleState.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblAngleState.Size = new System.Drawing.Size(64, 12);
            this.lblAngleState.TabIndex = 33;
            this.lblAngleState.Text = "Angle";
            this.lblAngleState.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbWiiChunkButtonC_Func
            // 
            this.cbWiiChunkButtonC_Func.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbWiiChunkButtonC_Func.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWiiChunkButtonC_Func.FormattingEnabled = true;
            this.cbWiiChunkButtonC_Func.Location = new System.Drawing.Point(147, 147);
            this.cbWiiChunkButtonC_Func.Name = "cbWiiChunkButtonC_Func";
            this.cbWiiChunkButtonC_Func.Size = new System.Drawing.Size(138, 20);
            this.cbWiiChunkButtonC_Func.TabIndex = 28;
            // 
            // cbWiiChunkButtonZ_Func
            // 
            this.cbWiiChunkButtonZ_Func.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbWiiChunkButtonZ_Func.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWiiChunkButtonZ_Func.FormattingEnabled = true;
            this.cbWiiChunkButtonZ_Func.Location = new System.Drawing.Point(147, 123);
            this.cbWiiChunkButtonZ_Func.Name = "cbWiiChunkButtonZ_Func";
            this.cbWiiChunkButtonZ_Func.Size = new System.Drawing.Size(138, 20);
            this.cbWiiChunkButtonZ_Func.TabIndex = 27;
            // 
            // cbWiiChunkAccelZ_Func
            // 
            this.cbWiiChunkAccelZ_Func.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbWiiChunkAccelZ_Func.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWiiChunkAccelZ_Func.FormattingEnabled = true;
            this.cbWiiChunkAccelZ_Func.Location = new System.Drawing.Point(147, 99);
            this.cbWiiChunkAccelZ_Func.Name = "cbWiiChunkAccelZ_Func";
            this.cbWiiChunkAccelZ_Func.Size = new System.Drawing.Size(138, 20);
            this.cbWiiChunkAccelZ_Func.TabIndex = 26;
            // 
            // cbWiiChunkAccelY_Func
            // 
            this.cbWiiChunkAccelY_Func.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbWiiChunkAccelY_Func.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWiiChunkAccelY_Func.FormattingEnabled = true;
            this.cbWiiChunkAccelY_Func.Location = new System.Drawing.Point(147, 75);
            this.cbWiiChunkAccelY_Func.Name = "cbWiiChunkAccelY_Func";
            this.cbWiiChunkAccelY_Func.Size = new System.Drawing.Size(138, 20);
            this.cbWiiChunkAccelY_Func.TabIndex = 25;
            // 
            // cbWiiChunkAccelX_Func
            // 
            this.cbWiiChunkAccelX_Func.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbWiiChunkAccelX_Func.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWiiChunkAccelX_Func.FormattingEnabled = true;
            this.cbWiiChunkAccelX_Func.Location = new System.Drawing.Point(147, 51);
            this.cbWiiChunkAccelX_Func.Name = "cbWiiChunkAccelX_Func";
            this.cbWiiChunkAccelX_Func.Size = new System.Drawing.Size(138, 20);
            this.cbWiiChunkAccelX_Func.TabIndex = 24;
            // 
            // cbWiiChunkJoyY_Func
            // 
            this.cbWiiChunkJoyY_Func.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbWiiChunkJoyY_Func.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWiiChunkJoyY_Func.FormattingEnabled = true;
            this.cbWiiChunkJoyY_Func.Location = new System.Drawing.Point(147, 27);
            this.cbWiiChunkJoyY_Func.Name = "cbWiiChunkJoyY_Func";
            this.cbWiiChunkJoyY_Func.Size = new System.Drawing.Size(138, 20);
            this.cbWiiChunkJoyY_Func.TabIndex = 23;
            // 
            // lblWiiChunkButtonC
            // 
            this.lblWiiChunkButtonC.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWiiChunkButtonC.Location = new System.Drawing.Point(4, 148);
            this.lblWiiChunkButtonC.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkButtonC.Name = "lblWiiChunkButtonC";
            this.lblWiiChunkButtonC.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkButtonC.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkButtonC.TabIndex = 21;
            this.lblWiiChunkButtonC.Text = "Button C:";
            this.lblWiiChunkButtonC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWiiChunkButtonC_State
            // 
            this.lblWiiChunkButtonC_State.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWiiChunkButtonC_State.Location = new System.Drawing.Point(76, 148);
            this.lblWiiChunkButtonC_State.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkButtonC_State.Name = "lblWiiChunkButtonC_State";
            this.lblWiiChunkButtonC_State.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkButtonC_State.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkButtonC_State.TabIndex = 20;
            this.lblWiiChunkButtonC_State.Text = "C";
            this.lblWiiChunkButtonC_State.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiiChunkButtonZ
            // 
            this.lblWiiChunkButtonZ.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWiiChunkButtonZ.Location = new System.Drawing.Point(4, 124);
            this.lblWiiChunkButtonZ.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkButtonZ.Name = "lblWiiChunkButtonZ";
            this.lblWiiChunkButtonZ.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkButtonZ.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkButtonZ.TabIndex = 19;
            this.lblWiiChunkButtonZ.Text = "Button Z:";
            this.lblWiiChunkButtonZ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWiiChunkButtonZ_State
            // 
            this.lblWiiChunkButtonZ_State.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWiiChunkButtonZ_State.Location = new System.Drawing.Point(76, 124);
            this.lblWiiChunkButtonZ_State.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkButtonZ_State.Name = "lblWiiChunkButtonZ_State";
            this.lblWiiChunkButtonZ_State.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkButtonZ_State.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkButtonZ_State.TabIndex = 18;
            this.lblWiiChunkButtonZ_State.Text = "Z";
            this.lblWiiChunkButtonZ_State.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiiChunkAccelY_Value
            // 
            this.lblWiiChunkAccelY_Value.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWiiChunkAccelY_Value.Location = new System.Drawing.Point(76, 76);
            this.lblWiiChunkAccelY_Value.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkAccelY_Value.Name = "lblWiiChunkAccelY_Value";
            this.lblWiiChunkAccelY_Value.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkAccelY_Value.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkAccelY_Value.TabIndex = 17;
            this.lblWiiChunkAccelY_Value.Text = "AccelY";
            this.lblWiiChunkAccelY_Value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiiChunkAccelZ_Value
            // 
            this.lblWiiChunkAccelZ_Value.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWiiChunkAccelZ_Value.Location = new System.Drawing.Point(76, 100);
            this.lblWiiChunkAccelZ_Value.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkAccelZ_Value.Name = "lblWiiChunkAccelZ_Value";
            this.lblWiiChunkAccelZ_Value.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkAccelZ_Value.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkAccelZ_Value.TabIndex = 16;
            this.lblWiiChunkAccelZ_Value.Text = "AccelZ";
            this.lblWiiChunkAccelZ_Value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiiChunkAccelX_Value
            // 
            this.lblWiiChunkAccelX_Value.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWiiChunkAccelX_Value.Location = new System.Drawing.Point(76, 52);
            this.lblWiiChunkAccelX_Value.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkAccelX_Value.Name = "lblWiiChunkAccelX_Value";
            this.lblWiiChunkAccelX_Value.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkAccelX_Value.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkAccelX_Value.TabIndex = 15;
            this.lblWiiChunkAccelX_Value.Text = "AccelX";
            this.lblWiiChunkAccelX_Value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiiChunkJoyY_Value
            // 
            this.lblWiiChunkJoyY_Value.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWiiChunkJoyY_Value.Location = new System.Drawing.Point(76, 28);
            this.lblWiiChunkJoyY_Value.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkJoyY_Value.Name = "lblWiiChunkJoyY_Value";
            this.lblWiiChunkJoyY_Value.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkJoyY_Value.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkJoyY_Value.TabIndex = 14;
            this.lblWiiChunkJoyY_Value.Text = "JoyY";
            this.lblWiiChunkJoyY_Value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiiChunkJoyX_Value
            // 
            this.lblWiiChunkJoyX_Value.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblWiiChunkJoyX_Value.Location = new System.Drawing.Point(76, 4);
            this.lblWiiChunkJoyX_Value.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkJoyX_Value.Name = "lblWiiChunkJoyX_Value";
            this.lblWiiChunkJoyX_Value.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkJoyX_Value.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkJoyX_Value.TabIndex = 13;
            this.lblWiiChunkJoyX_Value.Text = "JoyX";
            this.lblWiiChunkJoyX_Value.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWiiChunkAccelZ
            // 
            this.lblWiiChunkAccelZ.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWiiChunkAccelZ.Location = new System.Drawing.Point(4, 100);
            this.lblWiiChunkAccelZ.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkAccelZ.Name = "lblWiiChunkAccelZ";
            this.lblWiiChunkAccelZ.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkAccelZ.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkAccelZ.TabIndex = 12;
            this.lblWiiChunkAccelZ.Text = "Accel Z:";
            this.lblWiiChunkAccelZ.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWiiChunkAccelY
            // 
            this.lblWiiChunkAccelY.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWiiChunkAccelY.Location = new System.Drawing.Point(4, 76);
            this.lblWiiChunkAccelY.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkAccelY.Name = "lblWiiChunkAccelY";
            this.lblWiiChunkAccelY.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkAccelY.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkAccelY.TabIndex = 11;
            this.lblWiiChunkAccelY.Text = "Accel Y:";
            this.lblWiiChunkAccelY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWiiChunkAccelX
            // 
            this.lblWiiChunkAccelX.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWiiChunkAccelX.Location = new System.Drawing.Point(4, 52);
            this.lblWiiChunkAccelX.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkAccelX.Name = "lblWiiChunkAccelX";
            this.lblWiiChunkAccelX.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkAccelX.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkAccelX.TabIndex = 10;
            this.lblWiiChunkAccelX.Text = "Accel X:";
            this.lblWiiChunkAccelX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWiiChunkJoyY
            // 
            this.lblWiiChunkJoyY.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWiiChunkJoyY.Location = new System.Drawing.Point(4, 28);
            this.lblWiiChunkJoyY.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkJoyY.Name = "lblWiiChunkJoyY";
            this.lblWiiChunkJoyY.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkJoyY.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkJoyY.TabIndex = 8;
            this.lblWiiChunkJoyY.Text = "Joy Y:";
            this.lblWiiChunkJoyY.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblWiiChunkJoyX
            // 
            this.lblWiiChunkJoyX.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWiiChunkJoyX.Location = new System.Drawing.Point(4, 4);
            this.lblWiiChunkJoyX.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkJoyX.Name = "lblWiiChunkJoyX";
            this.lblWiiChunkJoyX.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkJoyX.Size = new System.Drawing.Size(64, 16);
            this.lblWiiChunkJoyX.TabIndex = 5;
            this.lblWiiChunkJoyX.Text = "Joy X:";
            this.lblWiiChunkJoyX.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbWiiChunkJoyX_Func
            // 
            this.cbWiiChunkJoyX_Func.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbWiiChunkJoyX_Func.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWiiChunkJoyX_Func.FormattingEnabled = true;
            this.cbWiiChunkJoyX_Func.Location = new System.Drawing.Point(147, 3);
            this.cbWiiChunkJoyX_Func.Name = "cbWiiChunkJoyX_Func";
            this.cbWiiChunkJoyX_Func.Size = new System.Drawing.Size(138, 20);
            this.cbWiiChunkJoyX_Func.TabIndex = 22;
            // 
            // lblWiiChunkChannel
            // 
            this.lblWiiChunkChannel.Dock = System.Windows.Forms.DockStyle.Left;
            this.lblWiiChunkChannel.Location = new System.Drawing.Point(4, 172);
            this.lblWiiChunkChannel.Margin = new System.Windows.Forms.Padding(4);
            this.lblWiiChunkChannel.Name = "lblWiiChunkChannel";
            this.lblWiiChunkChannel.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblWiiChunkChannel.Size = new System.Drawing.Size(60, 16);
            this.lblWiiChunkChannel.TabIndex = 29;
            this.lblWiiChunkChannel.Text = "Channel:";
            this.lblWiiChunkChannel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbWiiChunkChannel
            // 
            this.cbWiiChunkChannel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cbWiiChunkChannel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbWiiChunkChannel.FormattingEnabled = true;
            this.cbWiiChunkChannel.Location = new System.Drawing.Point(147, 171);
            this.cbWiiChunkChannel.Name = "cbWiiChunkChannel";
            this.cbWiiChunkChannel.Size = new System.Drawing.Size(138, 20);
            this.cbWiiChunkChannel.TabIndex = 30;
            // 
            // checkDataNormalize
            // 
            this.checkDataNormalize.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.checkDataNormalize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkDataNormalize.Location = new System.Drawing.Point(147, 195);
            this.checkDataNormalize.Name = "checkDataNormalize";
            this.checkDataNormalize.Size = new System.Drawing.Size(138, 14);
            this.checkDataNormalize.TabIndex = 31;
            this.checkDataNormalize.Text = "Data Normalized";
            this.checkDataNormalize.UseVisualStyleBackColor = true;
            // 
            // lblAngle
            // 
            this.lblAngle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblAngle.Location = new System.Drawing.Point(4, 196);
            this.lblAngle.Margin = new System.Windows.Forms.Padding(4);
            this.lblAngle.Name = "lblAngle";
            this.lblAngle.Padding = new System.Windows.Forms.Padding(4, 0, 0, 0);
            this.lblAngle.Size = new System.Drawing.Size(64, 12);
            this.lblAngle.TabIndex = 32;
            this.lblAngle.Text = "Angle:";
            this.lblAngle.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // status
            // 
            this.status.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.StatusLabel_Serial,
            this.btnActions});
            this.status.Location = new System.Drawing.Point(0, 440);
            this.status.Name = "status";
            this.status.ShowItemToolTips = true;
            this.status.Size = new System.Drawing.Size(564, 22);
            this.status.SizingGrip = false;
            this.status.TabIndex = 3;
            this.status.Text = "Ready";
            // 
            // StatusLabel_Serial
            // 
            this.StatusLabel_Serial.AutoSize = false;
            this.StatusLabel_Serial.Name = "StatusLabel_Serial";
            this.StatusLabel_Serial.Size = new System.Drawing.Size(517, 17);
            this.StatusLabel_Serial.Spring = true;
            this.StatusLabel_Serial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // btnActions
            // 
            this.btnActions.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnActions.DropDown = this.mnActions;
            this.btnActions.Image = ((System.Drawing.Image)(resources.GetObject("btnActions.Image")));
            this.btnActions.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnActions.Name = "btnActions";
            this.btnActions.Size = new System.Drawing.Size(32, 20);
            this.btnActions.Text = "toolStripSplitButton1";
            this.btnActions.ToolTipText = "Actions";
            this.btnActions.ButtonClick += new System.EventHandler(this.btnActions_ButtonClick);
            // 
            // mnActions
            // 
            this.mnActions.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.miActionsOpen,
            this.miActionsClose});
            this.mnActions.Name = "mnActions";
            this.mnActions.OwnerItem = this.btnActions;
            this.mnActions.Size = new System.Drawing.Size(151, 48);
            this.mnActions.ItemClicked += new System.Windows.Forms.ToolStripItemClickedEventHandler(this.mnActions_ItemClicked);
            // 
            // miActionsOpen
            // 
            this.miActionsOpen.Name = "miActionsOpen";
            this.miActionsOpen.Size = new System.Drawing.Size(150, 22);
            this.miActionsOpen.Text = "Open Device";
            // 
            // miActionsClose
            // 
            this.miActionsClose.Name = "miActionsClose";
            this.miActionsClose.Size = new System.Drawing.Size(150, 22);
            this.miActionsClose.Text = "Close Device";
            // 
            // bgWork
            // 
            this.bgWork.WorkerReportsProgress = true;
            this.bgWork.WorkerSupportsCancellation = true;
            this.bgWork.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bgWork_DoWork);
            this.bgWork.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.bgWork_ProgressChanged);
            this.bgWork.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.bgWork_RunWorkerCompleted);
            // 
            // timer
            // 
            this.timer.Enabled = true;
            this.timer.Interval = 50;
            this.timer.Tick += new System.EventHandler(this.timer_Tick);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(564, 462);
            this.Controls.Add(this.status);
            this.Controls.Add(this.grpWiiChunk);
            this.Controls.Add(this.grpDevice);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "Wii Nunchunk To MIDI";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.MainForm_FormClosed);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.MainForm_Paint);
            this.grpDevice.ResumeLayout(false);
            this.VPanelDevice.ResumeLayout(false);
            this.grpWiiChunk.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.status.ResumeLayout(false);
            this.status.PerformLayout();
            this.mnActions.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpDevice;
        private System.Windows.Forms.FlowLayoutPanel VPanelDevice;
        private System.Windows.Forms.Label lblDeviceIn;
        private System.Windows.Forms.ComboBox cbDeviceIn;
        private System.Windows.Forms.Label lblDeviceOut;
        private System.Windows.Forms.ComboBox cbDeviceOut;
        private System.Windows.Forms.Button btnDeviceDetect;
        private System.Windows.Forms.GroupBox grpWiiChunk;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label lblWiiChunkAccelZ;
        private System.Windows.Forms.Label lblWiiChunkAccelY;
        private System.Windows.Forms.Label lblWiiChunkAccelX;
        private System.Windows.Forms.Label lblWiiChunkJoyY;
        private System.Windows.Forms.Label lblWiiChunkJoyX;
        private System.Windows.Forms.Label lblWiiChunkAccelX_Value;
        private System.Windows.Forms.Label lblWiiChunkJoyY_Value;
        private System.Windows.Forms.Label lblWiiChunkJoyX_Value;
        private System.Windows.Forms.Label lblWiiChunkAccelY_Value;
        private System.Windows.Forms.Label lblWiiChunkAccelZ_Value;
        private System.Windows.Forms.Label lblWiiChunkButtonC;
        private System.Windows.Forms.Label lblWiiChunkButtonC_State;
        private System.Windows.Forms.Label lblWiiChunkButtonZ;
        private System.Windows.Forms.Label lblWiiChunkButtonZ_State;
        private System.Windows.Forms.Label lblSerial;
        private System.Windows.Forms.ComboBox cbSerial;
        private System.Windows.Forms.StatusStrip status;
        private System.Windows.Forms.ToolStripStatusLabel StatusLabel_Serial;
        private System.ComponentModel.BackgroundWorker bgWork;
        private System.Windows.Forms.ComboBox cbWiiChunkJoyX_Func;
        private System.Windows.Forms.ComboBox cbWiiChunkButtonC_Func;
        private System.Windows.Forms.ComboBox cbWiiChunkButtonZ_Func;
        private System.Windows.Forms.ComboBox cbWiiChunkAccelZ_Func;
        private System.Windows.Forms.ComboBox cbWiiChunkAccelY_Func;
        private System.Windows.Forms.ComboBox cbWiiChunkAccelX_Func;
        private System.Windows.Forms.ComboBox cbWiiChunkJoyY_Func;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.ToolStripSplitButton btnActions;
        private System.Windows.Forms.ContextMenuStrip mnActions;
        private System.Windows.Forms.ToolStripMenuItem miActionsOpen;
        private System.Windows.Forms.ToolStripMenuItem miActionsClose;
        private System.Windows.Forms.Label lblWiiChunkChannel;
        private System.Windows.Forms.ComboBox cbWiiChunkChannel;
        private System.Windows.Forms.CheckBox checkDataNormalize;
        private System.Windows.Forms.Label lblAngle;
        private System.Windows.Forms.Label lblAngleState;
    }
}

