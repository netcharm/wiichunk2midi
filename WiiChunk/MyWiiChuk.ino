#include <driverlib/sysctl.h>
#include <driverlib/watchdog.h>
#include <Wire.h>

const int NGnd = PB_4;
const int NVcc = PA_5;

static uint8_t NBuff[6];

static int dataMode = 1;
static int reset = 0;

void setup()
{
    HMIInit();
    Serial.begin(38400);
    Serial.print('\n');

    if(dataMode == LOW)
    {   
        Serial.println("\nWiiChuk Start");
    }

    WatchDogInit();
    
    PowerSetup();
    WiiNunchuckInit();
    reset = 0;
}

void loop()
{
    WatchDogFeed();   
    dataMode = digitalRead(PUSH1);
    if(reset == 0)
    {
      delay(Run());
    }
    else
    {
       PowerSetup();
       WiiNunchuckInit();       
       reset = 0; 
    }
}

void HMIInit()
{
    pinMode(RED_LED, OUTPUT);
    pinMode(GREEN_LED, OUTPUT);
    pinMode(BLUE_LED, OUTPUT);

    analogWrite(RED_LED, 0);
    analogWrite(GREEN_LED, 0);
    analogWrite(BLUE_LED, 0);
    
    pinMode(PUSH1, INPUT_PULLUP);
    pinMode(PUSH2, INPUT_PULLUP);  
    
    dataMode = digitalRead(PUSH1);
}

void WatchDogInit()
{
    // 准备定时 350ms
    //unsigned long ulValue = 350 * (SysCtlClockGet() / 1000);  
    unsigned long ulValue = 350 * (SysCtlClockGet() / 1000);  
    // 使能看门狗模块
    SysCtlPeripheralEnable(SYSCTL_PERIPH_WDOG0);
    // 使能看门狗复位功能
    WatchdogResetEnable(WATCHDOG0_BASE);
    // 使能调试器暂停看门狗计数
    WatchdogStallEnable(WATCHDOG0_BASE);
    // 设置看门狗装载值
    WatchdogReloadSet(WATCHDOG0_BASE, ulValue);
    // 使能看门狗
    WatchdogEnable(WATCHDOG0_BASE);
    // 锁定看门狗
    WatchdogLock(WATCHDOG0_BASE);
    
    if(dataMode == LOW)
    {
        Serial.println("Watchdog enabled!");
    }
}

void WatchDogFeed()
{
  // 解除锁定
  WatchdogUnlock(WATCHDOG_BASE);
  // 清除中断状态，即喂狗操作
  WatchdogIntClear(WATCHDOG_BASE);
  // 重新锁定  
  WatchdogLock(WATCHDOG_BASE);
}

void PowerOn()
{
    pinMode(NVcc, OUTPUT);
    pinMode(NGnd, OUTPUT);

    digitalWrite(NGnd, LOW);
    digitalWrite(NVcc, HIGH);
    
    analogWrite(RED_LED, 1);    
}

void PowerOff()
{
    pinMode(NVcc, OUTPUT);
    pinMode(NGnd, OUTPUT);

    digitalWrite(NGnd, LOW);
    digitalWrite(NVcc, LOW);

    analogWrite(RED_LED, 0);
    analogWrite(BLUE_LED, 0);
}

void PowerSetup()
{
    WatchDogFeed();
    
    PowerOff();
    delay(25);
    PowerOn();
    delay(25);

    if(dataMode == LOW)
    {
        Serial.println("Power Setting");
    }
}

void WiiNunchuckInit()
{
    WatchDogFeed();  
//    Serial.println("Wii Nunchuck Init...");
    
    Wire.setModule(1);
    Wire.begin();
    
    Wire.beginTransmission(0x52);
    Wire.write(0x40);
    Wire.write(0x00);
//    Wire.write(0xF0);
//    Wire.write(0x55);
    Wire.endTransmission();

    analogWrite(BLUE_LED, 1);
    delay(50);

    if(dataMode == LOW)
    {
        Serial.println("Wii Nunchuck Inited");
    }
}


void SendRequest()
{
    Wire.beginTransmission(0x52);
    Wire.write(0x00);
    Wire.endTransmission();
}


char Decode(char c)
{
    c = (c ^ 0x17) +0x17;
    return c;
}

int GetData()
{
    int count = 0;
//    int FFcount = 0;
    Wire.requestFrom(0x52, 6);
    if(dataMode == LOW)
    {
        Serial.println("Fetched new data[6]");
    }

    while(Wire.available())
    {
        NBuff[count] = Decode(Wire.read());
        count++;
        if(dataMode == LOW)
        {
            Serial.println("Read new data byte");
        }
        
/*        
        if(NBuff[count] == 0xff) 
        {
            FFcount++;
            if(FFcount>120)
            { 
                reset = 1;
                break;
            }
        }
        else
        {
            FFcount = 0;  
        }
*/        
    }

    if(count >= 5)
    {
        count = 0;
        if(dataMode == LOW)
        {
            debug();
        }
        else
        {
            dataOut();
        }
    }
    SendRequest();
    if(dataMode == LOW)
    {
        Serial.println("Fetched next data bytes");
    }
    return 0;    
}

void debug()
{
    int joy_x = NBuff[0];
    int joy_y = NBuff[1];

//    int accel_x = NBuff[2];
//    int accel_y = NBuff[3];
//    int accel_z = NBuff[4];

    word accel_x = NBuff[2] << 2;
    word accel_y = NBuff[3] << 2;
    word accel_z = NBuff[4] << 2;

    int button_c = 0;
    int button_z = 0;

    if((NBuff[5] >> 0) & 1){
        button_z = 1;
    }
    if((NBuff[5] >> 1) & 1){
        button_c = 1;
    }

    if((NBuff[5] >> 2) & 1){
        accel_x += 2;
    }
    if((NBuff[5] >> 3) & 1){
        accel_x += 1;
    }

    if((NBuff[5] >> 4) & 1){
        accel_y += 2;
    }
    if((NBuff[5] >> 5) & 1){
        accel_y += 1;
    }

    if((NBuff[5] >> 6) & 1){
        accel_z += 2;
    }
    if((NBuff[5] >> 7) & 1){
        accel_z += 1;
    }

    Serial.print("joy:");
    Serial.print(joy_x,DEC);
    Serial.print(", ");
    Serial.print(joy_y,DEC);
    Serial.print(" \t");
    Serial.print("accel:");
    Serial.print(accel_x,DEC);
    Serial.print(", ");
    Serial.print(accel_y,DEC);
    Serial.print(", ");
    Serial.print(accel_z,DEC);
    Serial.print(" \t");
    Serial.print("button:");
    Serial.print(button_z,DEC);
    Serial.print(", ");
    Serial.print(button_c,DEC);
    Serial.println("");     
}

void dataOut()
{
    int joy_x = NBuff[0];
    int joy_y = NBuff[1];

//    int accel_x = NBuff[2];
//    int accel_y = NBuff[3];
//    int accel_z = NBuff[4];

    word accel_x = NBuff[2] << 2;
    word accel_y = NBuff[3] << 2;
    word accel_z = NBuff[4] << 2;

    int button_c = 0;
    int button_z = 0;

    if((NBuff[5] >> 0) & 1){
        button_z = 1;
    }
    if((NBuff[5] >> 1) & 1){
        button_c = 1;
    }

    if((NBuff[5] >> 2) & 1){
        accel_x += 2;
    }
    if((NBuff[5] >> 3) & 1){
        accel_x += 1;
    }

    if((NBuff[5] >> 4) & 1){
        accel_y += 2;
    }
    if((NBuff[5] >> 5) & 1){
        accel_y += 1;
    }

    if((NBuff[5] >> 6) & 1){
        accel_z += 2;
    }
    if((NBuff[5] >> 7) & 1){
        accel_z += 1;
    }

    Serial.print(joy_x, DEC);
    Serial.print(",");
    Serial.print(joy_y, DEC);
    Serial.print(",");
    Serial.print(accel_x, DEC);
    Serial.print(",");
    Serial.print(accel_y,DEC);
    Serial.print(",");
    Serial.print(accel_z,DEC);
    Serial.print(",");
    Serial.print(button_z,DEC);
    Serial.print(",");
    Serial.print(button_c,DEC);
    Serial.print('\n');
}

int Run()
{
    analogWrite(GREEN_LED, 0);
    GetData();   
    analogWrite(GREEN_LED, 1);    
    return 75;
}


