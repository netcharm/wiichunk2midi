The application is freeware and no warranty.

Intro:
======
This application is a MIDI controller application. It can send MIDI
command to MIDI Port(real or virtual) with the Wii Nunchunk Joystick.

It is based on Stellaris LaunchPad(you can change to
MSP430 LaunchPad or Arduino and any other board) with WiiChunk Adaptor.

Features
========
1. Support limited MIDI command
2. User define the wiichunk parameters for calibration
3. auto save last setting for next startup

Usage:
======
1. compiling & flash the code to board with Energia/Arduino IDE(code must be modified).
2. Connected the nunchunk to board, and insert the board to PC
3. select the MIDI port to be sending out, COM port for board input
4. open the application, click bottom-right corner button to open port
5. setup your music software/device to receive the MIDI command.

Note:
======
1. App is in testing now, so some features not impl.
