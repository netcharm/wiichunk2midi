﻿using System;
using System.ComponentModel;
using System.Configuration;
using System.Drawing;
using System.Windows.Forms;

namespace WiiChunk2MIDI
{
    public partial class MainForm : Form
    {
        WiiChunk2MIDI_Helper WiiChunkMIDI = new WiiChunk2MIDI_Helper();

        private void LoadSetting()
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration( Application.ExecutablePath );
            AppSettingsSection appSection = config.AppSettings;

            if ( appSection.Settings["MidiIn"] != null )
            {
                WiiChunkMIDI.MidiIn = appSection.Settings["MidiIn"].Value;
            }

            if ( appSection.Settings["MidiOut"] != null )
            {
                WiiChunkMIDI.MidiOut = appSection.Settings["MidiOut"].Value;
            }

            if ( appSection.Settings["Serial"] != null )
            {
                WiiChunkMIDI.Serial = appSection.Settings["Serial"].Value;
            }

            if ( appSection.Settings["JoyX"] != null )
            {
                WiiChunk.JoyX_Func = appSection.Settings["JoyX"].Value;
            }
            if ( appSection.Settings["JoyY"] != null )
            {
                WiiChunk.JoyY_Func = appSection.Settings["JoyY"].Value;
            }

            if ( appSection.Settings["AccelX"] != null )
            {
                WiiChunk.AccelX_Func = appSection.Settings["AccelX"].Value;
            }
            if ( appSection.Settings["AccelY"] != null )
            {
                WiiChunk.AccelY_Func = appSection.Settings["AccelY"].Value;
            }
            if ( appSection.Settings["AccelZ"] != null )
            {
                WiiChunk.AccelZ_Func = appSection.Settings["AccelZ"].Value;
            }

            if ( appSection.Settings["ButtonZ"] != null )
            {
                WiiChunk.ButtonZ_Func = appSection.Settings["ButtonZ"].Value;
            }
            if ( appSection.Settings["ButtonC"] != null )
            {
                WiiChunk.ButtonC_Func = appSection.Settings["ButtonC"].Value;
            }

            if ( appSection.Settings["Channel"] != null )
            {
                WiiChunk.Channel = appSection.Settings["Channel"].Value;
            }

            if ( appSection.Settings["DataNormalized"] != null )
            {
                WiiChunk.DataToMIDI = Boolean.Parse( appSection.Settings["DataNormalized"].Value );
            }

        }

        private void SaveSetting()
        {
            WiiChunkMIDI.MidiIn = cbDeviceIn.Text;
            WiiChunkMIDI.MidiOut = cbDeviceOut.Text;
            WiiChunkMIDI.Serial = cbSerial.Text;

            WiiChunkMIDI.CheckDeviceAvaliable();

            Configuration config = ConfigurationManager.OpenExeConfiguration( Application.ExecutablePath );
            AppSettingsSection appSection = config.AppSettings;

            if ( appSection.Settings["MidiIn"] != null )
            {
                appSection.Settings["MidiIn"].Value = WiiChunkMIDI.MidiIn;
            }
            else
            {
                appSection.Settings.Add( "MidiIn", WiiChunkMIDI.MidiIn );
            }

            if ( appSection.Settings["MidiOut"] != null )
            {
                appSection.Settings["MidiOut"].Value = WiiChunkMIDI.MidiOut;
            }
            else
            {
                appSection.Settings.Add( "MidiOut", WiiChunkMIDI.MidiOut );
            }

            if ( appSection.Settings["Serial"] != null )
            {
                appSection.Settings["Serial"].Value = WiiChunkMIDI.Serial;
            }
            else
            {
                appSection.Settings.Add( "Serial", WiiChunkMIDI.Serial );
            }

            if ( appSection.Settings["JoyX"] != null )
            {
                appSection.Settings["JoyX"].Value = WiiChunk.JoyX_Func;
            }
            else
            {
                appSection.Settings.Add( "JoyX", WiiChunk.JoyX_Func );
            }
            if ( appSection.Settings["JoyY"] != null )
            {
                appSection.Settings["JoyY"].Value = WiiChunk.JoyY_Func;
            }
            else
            {
                appSection.Settings.Add( "JoyY", WiiChunk.JoyY_Func );
            }

            if ( appSection.Settings["AccelX"] != null )
            {
                appSection.Settings["AccelX"].Value = WiiChunk.AccelX_Func;
            }
            else
            {
                appSection.Settings.Add( "AccelX", WiiChunk.AccelX_Func );
            }
            if ( appSection.Settings["AccelY"] != null )
            {
                appSection.Settings["AccelY"].Value = WiiChunk.AccelY_Func;
            }
            else
            {
                appSection.Settings.Add( "AccelY", WiiChunk.AccelY_Func );
            }
            if ( appSection.Settings["AccelZ"] != null )
            {
                appSection.Settings["AccelZ"].Value = WiiChunk.AccelZ_Func;
            }
            else
            {
                appSection.Settings.Add( "AccelZ", WiiChunk.AccelZ_Func );
            }

            if ( appSection.Settings["ButtonZ"] != null )
            {
                appSection.Settings["ButtonZ"].Value = WiiChunk.ButtonZ_Func;
            }
            else
            {
                appSection.Settings.Add( "ButtonZ", WiiChunk.ButtonZ_Func );
            }
            if ( appSection.Settings["ButtonC"] != null )
            {
                appSection.Settings["ButtonC"].Value = WiiChunk.ButtonC_Func;
            }
            else
            {
                appSection.Settings.Add( "ButtonC", WiiChunk.ButtonC_Func );
            }

            if ( appSection.Settings["Channel"] != null )
            {
                appSection.Settings["Channel"].Value = WiiChunk.Channel;
            }
            else
            {
                appSection.Settings.Add( "Channel", WiiChunk.Channel );
            }

            if ( appSection.Settings["DataNormalized"] != null )
            {
                appSection.Settings["DataNormalized"].Value = WiiChunk.DataToMIDI.ToString();
            }
            else
            {
                appSection.Settings.Add( "DataNormalized", WiiChunk.DataToMIDI.ToString() );
            }

            config.Save( ConfigurationSaveMode.Full );
        }

        public MainForm()
        {
            InitializeComponent();
        }

        private void MainForm_Load( object sender, EventArgs e )
        {
            this.Icon = Icon.ExtractAssociatedIcon( Application.ExecutablePath );

            WiiChunkMIDI.CheckDevice();
            LoadSetting();

            cbWiiChunkJoyX_Func.Items.AddRange( WiiChunkMIDI.Controllers );
            cbWiiChunkJoyY_Func.Items.AddRange( WiiChunkMIDI.Controllers );
            cbWiiChunkAccelX_Func.Items.AddRange( WiiChunkMIDI.Controllers );
            cbWiiChunkAccelY_Func.Items.AddRange( WiiChunkMIDI.Controllers );
            cbWiiChunkAccelZ_Func.Items.AddRange( WiiChunkMIDI.Controllers );
            cbWiiChunkButtonZ_Func.Items.AddRange( WiiChunkMIDI.Controllers );
            cbWiiChunkButtonC_Func.Items.AddRange( WiiChunkMIDI.Controllers );

            cbWiiChunkJoyX_Func.SelectedIndex = cbWiiChunkJoyX_Func.Items.IndexOf( WiiChunk.JoyX_Func );
            cbWiiChunkJoyY_Func.SelectedIndex = cbWiiChunkJoyX_Func.Items.IndexOf( WiiChunk.JoyY_Func );
            cbWiiChunkAccelX_Func.SelectedIndex = cbWiiChunkJoyX_Func.Items.IndexOf( WiiChunk.AccelX_Func );
            cbWiiChunkAccelY_Func.SelectedIndex = cbWiiChunkJoyX_Func.Items.IndexOf( WiiChunk.AccelY_Func );
            cbWiiChunkAccelZ_Func.SelectedIndex = cbWiiChunkJoyX_Func.Items.IndexOf( WiiChunk.AccelZ_Func );
            cbWiiChunkButtonZ_Func.SelectedIndex = cbWiiChunkJoyX_Func.Items.IndexOf( WiiChunk.ButtonZ_Func );
            cbWiiChunkButtonC_Func.SelectedIndex = cbWiiChunkJoyX_Func.Items.IndexOf( WiiChunk.ButtonC_Func );

            cbWiiChunkChannel.Items.AddRange( WiiChunkMIDI.Channels );
            cbWiiChunkChannel.SelectedIndex = cbWiiChunkChannel.Items.IndexOf( WiiChunk.Channel );

            checkDataNormalize.Checked = WiiChunk.DataToMIDI;

            bgWork.ReportProgress( 1 );
        }

        private void MainForm_FormClosed( object sender, FormClosedEventArgs e )
        {
            WiiChunkMIDI.CloseDevice();
            SaveSetting();
        }

        private void timer_Tick( object sender, EventArgs e )
        {            
            bgWork.ReportProgress( 1 );
        }

        private void bgWork_DoWork( object sender, DoWorkEventArgs e )
        {
            // Update UI Info
            bgWork.ReportProgress( 1 );
        }

        private void bgWork_RunWorkerCompleted( object sender, RunWorkerCompletedEventArgs e )
        {
            //
        }

        private void bgWork_ProgressChanged( object sender, ProgressChangedEventArgs e )
        {
            try
            {
                if ( WiiChunkMIDI.UpdateWiiChunk )
                {
                    lblWiiChunkJoyX_Value.Text = WiiChunk.JoyX.Value.ToString();
                    lblWiiChunkJoyY_Value.Text = WiiChunk.JoyY.Value.ToString();

                    lblWiiChunkAccelX_Value.Text = WiiChunk.AccelX.Value.ToString();
                    lblWiiChunkAccelY_Value.Text = WiiChunk.AccelY.Value.ToString();
                    lblWiiChunkAccelZ_Value.Text = WiiChunk.AccelZ.Value.ToString();

                    if ( WiiChunk.ButtonZ.Value == 0 )
                    {
                        lblWiiChunkButtonZ_State.Text = "Pressed";
                    }
                    else
                    {
                        lblWiiChunkButtonZ_State.Text = "Released";
                    }
                    if ( WiiChunk.ButtonC.Value == 0 )
                    {
                        lblWiiChunkButtonC_State.Text = "Pressed";
                    }
                    else
                    {
                        lblWiiChunkButtonC_State.Text = "Released";
                    }

                    lblAngleState.Text = Enum.GetName( typeof(JoyPointType) , WiiChunk.JoyPoint() );
                }

                if ( WiiChunkMIDI.UpdateDevice )
                {
                    if ( !cbDeviceIn.DroppedDown )
                    {
                        WiiChunkMIDI.MidiIn = cbDeviceIn.Text;
                        cbDeviceIn.Items.Clear();
                        foreach ( String item in WiiChunkMIDI.MidiInList )
                        {
                            cbDeviceIn.Items.Add( item );
                        }
                        cbDeviceIn.Text = WiiChunkMIDI.MidiIn;
                    }

                    if ( !cbDeviceOut.DroppedDown )
                    {
                        WiiChunkMIDI.MidiOut = cbDeviceOut.Text;
                        cbDeviceOut.Items.Clear();
                        foreach ( String item in WiiChunkMIDI.MidiOutList )
                        {
                            cbDeviceOut.Items.Add( item );
                        }
                        cbDeviceOut.Text = WiiChunkMIDI.MidiOut;
                    }

                    if ( !cbSerial.DroppedDown )
                    {
                        WiiChunkMIDI.Serial = cbSerial.Text;
                        cbSerial.Items.Clear();
                        foreach ( String item in WiiChunkMIDI.SerialList )
                        {
                            cbSerial.Items.Add( item );
                        }
                        cbSerial.Text = WiiChunkMIDI.Serial;
                    }
                }

                if ( WiiChunkMIDI.UpdateFunction )
                {
                    if ( !cbWiiChunkJoyX_Func.DroppedDown )
                    {
                        WiiChunk.JoyX_Func = cbWiiChunkJoyX_Func.Text;
                    }
                    if ( !cbWiiChunkJoyY_Func.DroppedDown )
                    {
                        WiiChunk.JoyY_Func = cbWiiChunkJoyY_Func.Text;
                    }

                    if ( !cbWiiChunkAccelX_Func.DroppedDown )
                    {
                        WiiChunk.AccelX_Func = cbWiiChunkAccelX_Func.Text;
                    }
                    if ( !cbWiiChunkAccelY_Func.DroppedDown )
                    {
                        WiiChunk.AccelY_Func = cbWiiChunkAccelY_Func.Text;
                    }
                    if ( !cbWiiChunkAccelZ_Func.DroppedDown )
                    {
                        WiiChunk.AccelZ_Func = cbWiiChunkAccelZ_Func.Text;
                    }

                    if ( !cbWiiChunkButtonZ_Func.DroppedDown )
                    {
                        WiiChunk.ButtonZ_Func = cbWiiChunkButtonZ_Func.Text;
                    }
                    if ( !cbWiiChunkButtonC_Func.DroppedDown )
                    {
                        WiiChunk.ButtonC_Func = cbWiiChunkButtonC_Func.Text;
                    }

                    if ( !cbWiiChunkChannel.DroppedDown )
                    {
                        WiiChunk.Channel = cbWiiChunkChannel.Text;
                    }

                    if ( WiiChunkMIDI.IsOpen )
                    {
                        StatusLabel_Serial.Text = String.Format( "{0} Opened.", WiiChunkMIDI.Serial );
                    }
                    else
                    {
                        StatusLabel_Serial.Text = String.Format( "{0} Closed.", WiiChunkMIDI.Serial );
                    }

                    WiiChunk.DataToMIDI = checkDataNormalize.Checked;
                }
            }
            catch ( Exception ee )
            {

            }
        }

        private void MainForm_Paint( object sender, PaintEventArgs e )
        {
            WiiChunkMIDI.CheckDevice();
            bgWork.ReportProgress( 1 );
        }

        private void mnActions_ItemClicked( object sender, ToolStripItemClickedEventArgs e )
        {
            WiiChunkMIDI.MidiIn = cbDeviceIn.Text;
            WiiChunkMIDI.MidiOut = cbDeviceOut.Text;
            WiiChunkMIDI.Serial = cbSerial.Text;

            WiiChunkMIDI.CheckDeviceAvaliable();

            if ( e.ClickedItem.Name == miActionsOpen.Name )
            {
                WiiChunkMIDI.OpenDevice();
            }
            else if ( e.ClickedItem.Name == miActionsClose.Name )
            {
                WiiChunkMIDI.CloseDevice();
            }
            bgWork.ReportProgress( 1 );
        }

        private void btnActions_ButtonClick( object sender, EventArgs e )
        {
            WiiChunkMIDI.MidiIn = cbDeviceIn.Text;
            WiiChunkMIDI.MidiOut = cbDeviceOut.Text;
            WiiChunkMIDI.Serial = cbSerial.Text;

            WiiChunkMIDI.CheckDeviceAvaliable();

            if ( !WiiChunkMIDI.IsOpen )
            {
                WiiChunkMIDI.OpenDevice();
            }
            else
            {
                WiiChunkMIDI.CloseDevice();
            }
            bgWork.ReportProgress( 1 );
        }

        private void btnDeviceDetect_Click( object sender, EventArgs e )
        {
            WiiChunkMIDI.CheckDevice();
            bgWork.ReportProgress( 1 );
        }

    }


}
